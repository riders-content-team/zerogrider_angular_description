#ifndef GAZEBO_PLUGINS_ANGULARPLUGIN_HH_
#define GAZEBO_PLUGINS_ANGULARPLUGIN_HH_


#include <ignition/math/Pose3.hh>
#include <ignition/math/Vector3.hh>
#include "gazebo/physics/physics.hh"
#include <gazebo/sensors/sensors.hh>
#include "gazebo/common/common.hh"
#include "gazebo/gazebo.hh"

#include <ignition/transport/Node.hh>
#include <gazebo/transport/Node.hh>

#include <thread>
#include "ros/ros.h"
#include "std_msgs/String.h"
#include "std_msgs/Bool.h"
#include "geometry_msgs/Twist.h"
#include "geometry_msgs/Wrench.h"
#include "ros/callback_queue.h"
#include "ros/subscribe_options.h"

#include <math.h>


#define PI 3.14159265

namespace gazebo
{

  class GAZEBO_VISIBLE AngularPlugin : public ModelPlugin
  {
    // Constructor
    public: AngularPlugin();
    // Deconstructor
    public: virtual ~AngularPlugin();
    // Load Function
    // Runs Once on Initialization
    public: virtual void Load(physics::ModelPtr _model, sdf::ElementPtr /*_sdf*/);
    // Update Function
    // Updates on Every World Update
    protected: void onAngularReceive(const geometry_msgs::Wrench::ConstPtr &msg);
    
    // Connection to Contact Update events.
    private: event::ConnectionPtr contactConnection;
    // Connection to World Update events.
    private: event::ConnectionPtr worldConnection;
    // Node Used for Communication.
    private: std::mutex mutex;
    // Pointer to the World
    private: physics::WorldPtr world;
    // Pointer to the Model
    private: physics::ModelPtr model;
    // Pointer to the Link
    private: physics::Link_V links;
    private: physics::LinkPtr main_link;
    //Initial Pose of Model
    private: ignition::math::Pose3d pose;
    private: ignition::math::Pose3d currentPose;
    // Name of the Model
    private: std::string force_name;
    ///Set the linear velocity of the body.
    public: virtual void SetForce(const ignition::math::Vector3d &_vel);
    
    ///Set the torque applied to the body.
    public: virtual void SetTorque(const ignition::math::Vector3d &_torque);

    //set the position of the robot in the world
    public: void SetLinkWorldPose(const ignition::math::Pose3d &_pose,std::string _linkName);

    /// Set the angular velocity of the body.
    // public: virtual void SetAngularVel(const ignition::math::Vector3d &_vel);
    //ROS Node Handler
    private: std::unique_ptr<ros::NodeHandle> rosNode;
    //ROS publisher
    private: ros::Publisher angular_pub;
    private: ros::Subscriber rosSub;
    // Robot pose subscriber
    private: ros::Subscriber robotsubscriber;

    private: double z_phase;
  };
}
#endif

