#ifndef _GAZEBO_CONTACT_PLUGIN_HH_
#define _GAZEBO_CONTACT_PLUGIN_HH_

#include <string>

#include <gazebo/gazebo.hh>
#include <gazebo/sensors/sensors.hh>
#include "ros/ros.h"
#include "std_msgs/Bool.h"

namespace gazebo
{
  //An example plugin for a contact sensor.
  class ContactPlugin : public SensorPlugin
  {
    //Constructor.
    public: ContactPlugin();

    // Destructor.
    public: virtual ~ContactPlugin();

    //Load the sensor plugin.
    // _sensor Pointer to the sensor that loaded this plugin.
    // _sdf SDF element that describes the plugin.
    public: virtual void Load(sensors::SensorPtr _sensor, sdf::ElementPtr _sdf);

    //Callback that receives the contact sensor's update signal.
    private: virtual void OnUpdate();

    // Pointer to the contact sensor
    private: sensors::ContactSensorPtr parentSensor;

    //Connection that maintains a link between the contact sensor's
    // updated signal and the OnUpdate callback.
    private: event::ConnectionPtr updateConnection;
    private: std::unique_ptr<ros::NodeHandle> rosNode;
    private: ros::Publisher contact_pub;
  };
}
#endif