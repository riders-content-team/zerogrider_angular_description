#include "AngularPlugin.hh"

using namespace gazebo;
GZ_REGISTER_MODEL_PLUGIN(AngularPlugin)

//current xyz poses
double cur_x_pose, cur_y_pose, cur_z_pose;

// Constructor
AngularPlugin::AngularPlugin() : ModelPlugin(){}

// Deconstructor
AngularPlugin::~AngularPlugin(){}

// Runs once on initialization
void AngularPlugin::Load(physics::ModelPtr _model, sdf::ElementPtr _sdf){
  // Model Pointer
  this->model = _model;
  this->force_name = this->model->GetName();
  // World Pointer Model is in
  this->world = this->model->GetWorld();
  // Link Pointer to Link
  this->links = this->model->GetLinks();
  this->pose = this->model->RelativePose();
  this->z_phase = (double)(rand() % 6283)/1000;

  // Initialize ros, if it has not already bee initialized.
  if (!ros::isInitialized()){
    int argc = 0;
    char **argv = NULL;
    ros::init(argc, argv, this->force_name,
        ros::init_options::NoSigintHandler);
  }
  // Create our ROS node. This acts in a similar manner to
  // the Gazebo node
  this->rosNode.reset(new ros::NodeHandle());
  //publisher for docking control
  this->angular_pub = this->rosNode->advertise<std_msgs::Bool>("dock",1000);
  
  //topic for controlling robot
  std::string topic = "robot_angular";
  ros::SubscribeOptions so =
    ros::SubscribeOptions::create<geometry_msgs::Wrench>(
        topic,
        10,
        boost::bind(&AngularPlugin::onAngularReceive, this, _1),ros::VoidConstPtr(),NULL);
  this->robotsubscriber = this->rosNode->subscribe(so);
}

// Update Callback
void AngularPlugin::onAngularReceive(const geometry_msgs::Wrench::ConstPtr &msg){
  AngularPlugin::SetForce(ignition::math::Vector3d(msg->force.x, msg->force.y, msg->force.z));
  AngularPlugin::SetTorque(ignition::math::Vector3d(msg->torque.x, msg->torque.y, msg->torque.z));
  this->currentPose = this->model->WorldPose();
  cur_x_pose = this->currentPose.Pos().X();
  cur_y_pose = this->currentPose.Pos().Y();
  cur_z_pose = this->currentPose.Pos().Z();
  
  if( std::abs(cur_x_pose - (0.34)) <1 &&
      std::abs(cur_y_pose - (-24.017))<1 && 
      std::abs(cur_z_pose - (-0.393))<1){
    AngularPlugin::SetLinkWorldPose(ignition::math::Pose3d(0,-24,0,0,0,-1.57),"link");
    this->model->ResetPhysicsStates();
    std_msgs::Bool msg;
    msg.data = true;
    this->angular_pub.publish(msg);
  }
}

//set linear part of Wrench message which is Force
void AngularPlugin::SetForce(const ignition::math::Vector3d &force){
  //std::cout<<"force_x: "<<force[0]<<std::endl;
  //std::cout<<"force_y: "<<force[1]<<std::endl;
  //std::cout<<"force_z: "<<force[2]<<std::endl;

  for(std::vector<physics::LinkPtr>::iterator li = links.begin(); li != links.end(); ++li){
    if((*li)->GetName()=="link"){
      (*li)->SetForce(force);
    }
  }
  if(ros::ok()){
    ros::spinOnce();
  }
}
//set angular part of Wrench message which is torque
void AngularPlugin::SetTorque(const ignition::math::Vector3d &torque){
  //std::cout<<"angular x: "<<torque[0]<<std::endl;
  //std::cout<<"angular y: "<<torque[1]<<std::endl;
  //std::cout<<"angular z: "<<torque[2]<<std::endl;

  for(std::vector<physics::LinkPtr>::iterator li = links.begin(); li != links.end(); ++li){
    if((*li)->GetName()=="link"){
      (*li)->SetTorque(torque);
    }
  }
  if(ros::ok()){
    ros::spinOnce();
  }
}

void AngularPlugin::SetLinkWorldPose(const ignition::math::Pose3d &_pose, std::string _linkName){
  // look for link matching link name
  
   this->main_link = this->model->GetLink(_linkName);
  if (main_link)
    this->model->SetLinkWorldPose(_pose, main_link);
  else
    gzerr << "Setting Model Pose by specifying Link failed:"
          << " Link[" << _linkName << "] not found.\n";
}










